# Pokédex

<p align="center">
    <img src="https://www.notion.so/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2F7a7c6231-4c32-4108-b296-bdc2c829403d%2Fpokespartans.png?table=block&id=985a2b71-af04-4054-b9ec-7de7a7ed2863&width=250&cache=v2"  alt="">
</p>

<p align="center">
	<img src="https://img.shields.io/badge/npm-6.14.4-blue"  alt="npm badge">
	<img src="https://img.shields.io/badge/license-MIT-green"  alt="license badge">
	<img src="https://img.shields.io/badge/react-16.13.1-blue"  alt="react bagde">
	<img src="https://img.shields.io/badge/version-1.0.0-green"  alt="version bagde">
</p>

<p align="center">
    <img alt="GitHub followers" src="https://img.shields.io/github/followers/Esteban-Ladino?label=Esteban%20Ladino&style=social">
    <img alt="GitHub followers" src="https://img.shields.io/github/followers/emmaisworking?label=emmaIsWorking&style=social">
    <img alt="Twitter URL" src="https://img.shields.io/twitter/url?label=EmmaIsWorking&style=social&url=https%3A%2F%2Ftwitter.com%2Femmaisworking">
</p>

Pokedex frontend
A site where to find the characteristics of your favorites pokemons. This is a project of the Platzi Master program

## Demo

If you want to see the demo of this project deployed, you can visit it here

## How to clone
You can clone the repository

    $ git clone https://gitlab.com/pokespartans/frontend/pokedex.git
    
## Installation
To install this project just type

    $ npm install

To excecute type

    $ npm run start

To build type

    $ npm run build

## Preview

![preview](https://lh5.googleusercontent.com/-qZMumy1gDZtGtztd9ZmzhvBKTMwVOLmGGSKK5_pPZP3mUJQCrREX5O1cqwgWnS5Bd4q1CzgB3EIhIIeu_3o=w1920-h937)

## How to contribute

You can create a Pull request to the project

## License

soon