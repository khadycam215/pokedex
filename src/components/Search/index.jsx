import React from 'react'
import { Input, H1, Container } from './styles'

export const Search = ({ handleSearch }) => {
  return (
    <Container>
      <H1>800 <strong>Pokemons</strong> for you to choose your favorite</H1>
      <Input type='text' placeholder='Find your pokémon...' onChange={handleSearch} />
    </Container>
  )
}
