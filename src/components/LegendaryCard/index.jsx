import React from 'react'
import { Container, Img, Card, Pokeball } from './styles'

import PokeballIcon from '../../assets/static/pokeballGold.svg'

export const LegendaryCard = (props) => {
  const {
    id,
    name = 'mew',
    image = 'https://vignette.wikia.nocookie.net/es.pokemon/images/b/bf/Mew.png/revision/latest/scale-to-width-down/1000?cb=20160311010530'
  } = props

  return (
    <Container tabIndex='0' role='listitem' aria-label='List item'>
      <Img tabIndex='-1' role='figure' aria-label='Pokemon preview'>
        <img
          src={image}
          alt={`Legendary pokemon #${id}`}
        />
      </Img>

      <Card>
        <h3>{name}</h3>
        <Pokeball>
          <img src={PokeballIcon} alt='golden pokeball icon' />
        </Pokeball>
      </Card>
    </Container>
  )
}
