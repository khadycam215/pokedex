import React from 'react'
import Slider from 'infinite-react-carousel'

import { LegendaryCard } from '../LegendaryCard'
import { useResize } from '../../hook/useResize'

import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from 'react-icons/md'
import { Container } from './styles'

export const Carousel = () => {
  const [width, ref] = useResize()

  const settings = {
    className: 'carousel',
    slidesToShow: width > 1024 ? 7 : width > 768 ? 5 : width > 600 ? 3 : width > 374 ? 2 : 1,
    initialSlide: 1
  }

  return (
    <Container role='list' ref={ref} aria-label='Pokemon carousel'>
      <Slider
        {...settings}
        prevArrow={<MdKeyboardArrowLeft style={{ outline: '3px solid red' }} tabIndex='0' title='Arrow left' aria-label='Arrow left' alt='arrow left' />}
        nextArrow={<MdKeyboardArrowRight tabIndex='0' title='Arrow right' aria-label='Arrow right' alt='arrow right' />}
      >
        {
          [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            .map(card => <LegendaryCard aria-label='Card' key={card} />)
        }
      </Slider>
    </Container>
  )
}
