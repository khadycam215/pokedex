import { createGlobalStyle } from 'styled-components'

export const ModalStyles = createGlobalStyle`
  & .ModalCard {
    position: absolute;
    top: 50%;
    left: 50%;
    width: fit-content;
    margin-left: -350px;
    margin-top: -173px;
    background-color: transparent;
    border-radius: 5px;
    outline: none;
    user-select: none;
  }

  & .OverlayCard {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 100;
    background: ${({ theme }) => theme.backgroundModal};
  }

  & .closeButtonCard {
    position: absolute;
    top: -50px;
    right: 0;
    cursor: pointer;
  }
`
