import React from 'react'
import Modal from 'react-modal'
import { ModalStyles } from './styles'
import { PokemonCard } from '../PokemonCard'
import { MdClose } from 'react-icons/md'
import { Loading } from '../Loading'
import { ErrorLoading } from '../ErrorLoading'

export const ModalCard = (props) => {
  const {
    data: {
      loading,
      error,
      pokemonsSearch
    },
    showModal,
    handleShowModal
  } = props

  if (!showModal) return <></>
  if (loading) return <Loading />
  if (error) return <ErrorLoading />

  const {
    edges = []
  } = pokemonsSearch || {}

  if (edges.length === 0) return <ErrorLoading message={'Sorry, we couldn\'t find the pokemons'} />

  const pokemon = edges[0].node

  const handleModal = (event) => {
    event.persist()
    handleShowModal()
  }

  return (
    <>
      <ModalStyles />
      <Modal
        isOpen={showModal}
        className='ModalCard'
        onRequestClose={handleModal}
        overlayClassName='OverlayCard'
        ariaHideApp={false}
      >
        <PokemonCard {...pokemon} />
        <MdClose size='40' className='closeButtonCard' onClick={handleModal} />
      </Modal>
    </>
  )
}
