import styled from 'styled-components'

export const Container = styled.div`
  margin-top: ${({ theme }) => theme.spacingB};
  padding:${({ theme }) => theme.paddingS};

  & p {
    width: ${({ value, max }) => (100 * value) / max}%;
    color: ${({ theme }) => theme.colorGrey};
    margin-bottom: ${({ theme }) => theme.spacingS};

    &[data-value] {
      position: relative;

      &:after {
        content: attr(data-value);
        position: absolute;
        right: 0;
        bottom: -15px;
      }
    }
  }

  & progress[value] {
    width: 100%;
    height: 10px;
    border-radius: 5px;
    border: none;
    background-color: whiteSmoke;

    &::-webkit-progress-bar {
      border-radius: 5px;
      box-shadow: 0 2px 3px rgba(0, 0, 0, 0.5) inset;
      background-color: whiteSmoke;
    }

    &::-webkit-progress-value {
      background-image: -webkit-linear-gradient(rgba(255, 255, 255, 0.25),rgba(0, 0, 0, 0.2)),
      -webkit-linear-gradient(left, ${({ theme }) => theme.colorThird}, ${({ theme }) => theme.colorDanger});
      border-radius:5px;
    }

    &::-moz-progress-bar {
      border: 0;
      border-radius: 5px;
      background-size: 35px 20px, 100% 100%, 100% 100%;
      -moz-linear-gradient(rgba(255, 255, 255, .25), rgba(0, 0, 0, .2)), -moz-linear-gradient( left, ${({ theme }) => theme.colorThird}, ${({ theme }) => theme.colorDanger});
    }
  }

  @media(max-width: 768px) {
    margin-top: ${({ theme }) => theme.spacingS};
  }
`
