import styled from 'styled-components'

export const Section = styled.section`
display:flex;
flex-direction:column;
align-items:flex-start;
justify-content:center;
width:100%;
max-width: ${({ theme }) => theme.containerMaxWidth};

`

export const LegendTitle = styled.div`
display:flex;
flex-direction:column;
width:100%;
margin:${({ theme }) => theme.spacingM} auto;
&>h1{
  color:${({ theme }) => theme.colorGrey};
  font-size:${({ theme }) => theme.sizeFontTitle};
  margin:${({ theme }) => theme.spacingM} 0;
}


`
export const Legend = styled.div`
display:flex;
@media(max-width:425px){
  flex-direction:column;
}

`
export const Img = styled.figure`
display:flex;
align-items:center;
width:60%;

&>img{
  width:100%;
}

@media(max-width:425px){
  width:100%;
}
`

export const Description = styled.div`
display:flex;
flex-direction:column;
width:40%;
&>h2{
  color:${({ theme }) => theme.colorGrey};
  font-size:${({ theme }) => theme.sizeFontCardTitleImg};
}
&>p{
  color:${({ theme }) => theme.colorGrey};
  font-size:${({ theme }) => theme.sizeFontParagraph};
}
@media(max-width:425px){
  width:100%;
}
`

export const Features = styled.div`
display:grid;
grid-template-columns: 1fr 1fr;
`
