import React from 'react'
import { Section, LegendTitle, Legend, Img, Description, Features } from './styles'
import { SparkLineFeature } from '../SparkLineFeature'
import { Carousel } from '../Carousel'

export const LegendaryHero = (props) => {
  const {
    section = 'Legends',
    id = '-1',
    name = 'Mew',
    image = 'https://vignette.wikia.nocookie.net/es.pokemon/images/b/bf/Mew.png/revision/latest/scale-to-width-down/1000?cb=20160311010530',
    description = 'Mew es un Pokémon singular de tipo psíquico introducido en la primera generación. Es el ancestro de todos los Pokémon, ya que tiene todos los genes de los Pokémon existentes. Además, es el único Pokémon capaz de aprender todas las máquinas técnicas (MT), máquinas ocultas (MO) y movimientos del tutor de movimientos.',
    hp = 100,
    experienceGrowth = 1059860,
    attack = 100,
    defense = 100,
    spAttack = 100,
    spDefense = 100
  } = props

  return (
    <Section aria-label='Legendary Pokemon Section'>
      <LegendTitle aria-label='Tittle'>
        <h1>{section}</h1>
        <hr title={`Legendary Pokemon Section ${section}`} />
      </LegendTitle>
      <Legend>
        <Img role='img' aria-label='Legendary Pokemon'>
          <img
            src={image}
            alt={`image of legendary pokémon #${id}`}
          />
        </Img>
        <Description>
          <h2>{name}</h2>
          <p>{description}</p>
          <Features>
            <SparkLineFeature
              name='Healthy Points'
              max='255'
              value={hp}
            />
            <SparkLineFeature
              name='Experience'
              max='1640000'
              value={experienceGrowth}
            />
            <SparkLineFeature
              name='Attack'
              max='185'
              value={attack}
            />
            <SparkLineFeature
              name='Defense'
              max='230'
              value={defense}
            />
            <SparkLineFeature
              name='Special Attack'
              max='194'
              value={spAttack}
            />
            <SparkLineFeature
              name='Special Defense'
              max='230'
              value={spDefense}
            />
          </Features>
        </Description>
      </Legend>
      <Carousel />
    </Section>
  )
}
