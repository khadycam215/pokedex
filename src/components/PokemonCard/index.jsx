import React from 'react'
import {
  Container,
  Img,
  Tags,
  Description,
  TitleSection,
  DetailsPokemons,
  Id,
  CardContainer,
  HealthyContainer,
  Features,
  FeatureContainer
} from './styles'

import { SparkCicleFeature } from '../SparkCircleFeature'
import { Tag } from '../Tag'
import { SparkLineFeature } from '../SparkLineFeature'

export const PokemonCard = (props) => {
  const {
    name = 'Charizard',
    image = 'https://vignette.wikia.nocookie.net/es.pokemon/images/9/95/Charizard.png/revision/latest/scale-to-width-down/1000?cb=20180325003352',
    attack = 165,
    spAttak = 130,
    defense = 49,
    spDefense = 271,
    type1 = { name: 'Grass', color: '#30D97E' },
    type2 = { name: 'Poison', color: '#7A30D9' },
    abilities = 'Overgrow,Chlorophyll',
    hp = 75,
    experienceGrowth = 1250000,
    generation = 1,
    pokedexNumber = 578
  } = props

  console.log(props)

  return (
    <Container>
      <Img color={type1.color}>
        <img
          src={image}
          alt={`Imagen de poquemon ${name}`}
        />
        <Tags>
          <Tag {...type1} />
          {type2.name !== 'no-type' && <Tag {...type2} />}
        </Tags>
      </Img>

      <Description>
        <TitleSection>
          <h3>{name}</h3>
          <DetailsPokemons>
            <h3>Generation {generation}</h3>
            <Id>
              <p>{pokedexNumber}</p>
            </Id>
          </DetailsPokemons>
        </TitleSection>

        <CardContainer>
          <h4>Abilities</h4>
          <p>{abilities.replace(/,/g, ' - ')}</p>
        </CardContainer>

        <CardContainer>
          <HealthyContainer>
            <SparkLineFeature
              name='Healthy'
              max='255'
              value={hp}
            />
            <SparkLineFeature
              name='Experience'
              max='1640000'
              value={experienceGrowth}
            />
          </HealthyContainer>
        </CardContainer>

        <Features>
          <FeatureContainer>
            <SparkCicleFeature value={attack} name='Attack' />
          </FeatureContainer>
          <FeatureContainer>
            <SparkCicleFeature value={defense} name='Defense' />
          </FeatureContainer>
          <FeatureContainer>
            <SparkCicleFeature value={spAttak} name='SP Attack' />
          </FeatureContainer>
          <FeatureContainer>
            <SparkCicleFeature value={spDefense} name='Sp Defense' />
          </FeatureContainer>
        </Features>
      </Description>
    </Container>
  )
}
