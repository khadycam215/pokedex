import styled from 'styled-components'
export const Container = styled.div`
position:relative;
display: flex;
flex-direction:column;
justify-content: flex-start;
align-items: center;
margin: 0 auto;
box-sizing:border-box;
border-radius: ${({ theme }) => theme.borderM};
box-shadow: ${({ theme }) => theme.shadowCard};
width: 100%;
background-color: ${({ theme }) => theme.colorGrey};
`
export const Img = styled.figure`
position:relative;
left:0;
overflow: hidden;
border-radius:${({ theme }) => theme.borderM} ${({ theme }) => theme.borderM} 0 0;
width: 100%;

& > img {
object-fit: cover;
width: 100%;
height:300px;
}
`

export const Description = styled.div`
box-sizing:border-box;
max-width:100%;
width:100%;
padding: ${({ theme }) => theme.paddingM};

&>*{
display:flex;
flex-direction:column;
justify-content:center;
text-align:center;
}
`

export const TitleSection = styled.div`
display: flex;
justify-content: space-between;
width: 100%;
align-items: center;

& > h3 {
color: ${({ theme }) => theme.colorParagraph};
font-size: ${({ theme }) => theme.sizeFontSubtitle};
}
`

export const CardContainer = styled.div`
display: flex;
flex-direction: column;
align-items: start;
justify-content: center;
margin: ${({ theme }) => theme.spacingL} 0;
border-radius: ${({ theme }) => theme.borderXs};
box-shadow: ${({ theme }) => theme.shadowXs};
box-sizing:border-box;
width: 100%;
height: auto;
padding: 0 ${({ theme }) => theme.paddingS};
background-color: ${({ theme }) => theme.colorGrey};

&>p{
padding:${({ theme }) => theme.paddingS};
}
`
export const SocialMedia = styled.figure`
display:flex;
align-items:center;
justify-content:center;
margin: ${({ theme }) => theme.spacingS} auto;

& > *{
margin:0 8px;
color:${({ theme }) => theme.colorParagraph};
}
`
