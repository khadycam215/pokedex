import React from 'react'
import { FaGithubSquare, FaTwitterSquare } from 'react-icons/fa'

import { Container, Img, Description, TitleSection, CardContainer, SocialMedia } from './styles'

export const TeamCard = ({ src, alt, name, description, srcSmT, srcSmHb }) => {
  return (
    <Container tabIndex='0'>
      <Img role='img'>
        <img src={src} alt={alt} />
      </Img>

      <Description aria-label='Name'>
        <TitleSection>
          <h3>{name}</h3>
        </TitleSection>

        <CardContainer aria-label='Description'>
          <p>{description}</p>
        </CardContainer>

        <CardContainer aria-label='Social Media'>
          <SocialMedia>
            <h3>Follow me: </h3>
            <a
              tabIndex='0'
              aria-label='button Twitter'
              href={srcSmT}
              alt='Link for twitter social network'
            >
              <FaTwitterSquare size='30' />
            </a>
            <a
              tabIndex='0'
              aria-label='button GitHub'
              href={srcSmHb}
              alt='Link for Github social network'
            >
              <FaGithubSquare size='30' />
            </a>
          </SocialMedia>
        </CardContainer>
      </Description>
    </Container>
  )
}
