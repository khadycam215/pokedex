import React, { useState } from 'react'
import { FilterContainer } from '../ListFilter/styles'
import { MdKeyboardArrowUp, MdExpandMore } from 'react-icons/md'
import { Dl } from './styles'
import { Tag } from '../Tag'

export const RangesFilter = (props) => {
  const {
    name,
    open,
    handleOpen,
    handleFilter
  } = props

  const [from, setFrom] = useState('')
  const [to, setTo] = useState('')
  const [error, setError] = useState('')

  const handleFrom = (event) => setFrom(event.target.value)
  const handleTo = (event) => setTo(event.target.value)

  const handleSubmit = (event) => {
    event.preventDefault()
    if (from > to || !from || !to) {
      setError('Error')
      return
    }
    name === 'Attack'
      ? handleFilter({ attack_Gt: from, attack_Lt: to })
      : handleFilter({ experienceGrowth_Gt: from, experienceGrowth_Lt: to })
    setFrom('')
    setTo('')
    handleOpen()
  }

  return (
    <Dl display={open ? 'block' : 'none'}>
      <dt>
        <FilterContainer onClick={handleOpen}>
          <p>{name}</p>
          {open ? <MdKeyboardArrowUp /> : <MdExpandMore />}
        </FilterContainer>
      </dt>
      <dd>
        <form onSubmit={handleSubmit}>
          {error &&
            <>
              <p>{error}</p>
              <br />
            </>}
          <label>From
            <input type='number' value={from} onChange={handleFrom} />
          </label>
          <label>To
            <input type='number' value={to} onChange={handleTo} />
          </label>
          <button type='submit'><Tag name='Apply' /></button>
        </form>
      </dd>
    </Dl>
  )
}
