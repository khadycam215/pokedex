import styled from 'styled-components'

export const Dl = styled.dl`
  & dd {
    position: relative;
    & form {
      position: absolute;
      top: 10px;
      z-index: 1;
      display: ${({ display }) => display};
      width: 189px;
      height: auto;
      padding: ${({ theme }) => theme.paddingM};
      border-radius: ${({ theme }) => theme.borderXs};
      box-shadow: ${({ theme }) => theme.shadowXs};
      background-color: ${({ theme }) => theme.colorGrey};

      & label {
        margin-bottom: ${({ theme }) => theme.spacingXs};
        font-family: ${({ theme }) => theme.fontParagraphs};
      }
      & input {
        width: 145px;
        margin-top: ${({ theme }) => theme.spacingXs};
        margin-bottom: ${({ theme }) => theme.spacingS};
        padding: ${({ theme }) => theme.paddingXs} ${({ theme }) => theme.paddingM};
        border: none;
        border-radius: ${({ theme }) => theme.borderM};
        box-shadow: ${({ theme }) => theme.shadowXs};
        outline: none;
      }
      & button {
        margin-top: ${({ theme }) => theme.spacingS};
        border: none;
        background: transparent;
        outline: none;
        cursor: pointer;
      }
    }
  }
`
