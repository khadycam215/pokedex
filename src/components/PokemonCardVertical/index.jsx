import React from 'react'
import {
  Container,
  Img,
  CardContainer,
  Description,
  Tags,
  Details,
  DetailsPokemons,
  Id,
  Card,
  Features,
  FeatureContainer
} from './styles'

import { SparkCicleFeature } from '../SparkCircleFeature'
import { Tag } from '../Tag'

export const PokemonCardVertical = () => {
  return (
    <Container>
      <h3>Charizard</h3>
      <Img>
        <img
          src='https://vignette.wikia.nocookie.net/es.pokemon/images/9/95/Charizard.png/revision/latest/scale-to-width-down/1000?cb=20180325003352'
          alt='Imagen de poquemon #'
        />
      </Img>

      <CardContainer>
        <Description>
          <Details>
            <Tags>
              <Tag name='Grass' />
              <Tag name='Poison' />
            </Tags>
            <DetailsPokemons>
              <h3>Generation 1</h3>
              <Id>
                <p>578</p>
              </Id>
            </DetailsPokemons>
          </Details>

          <Card>
            <h4>Abilities</h4>
            <p>Overgrow - Chlorophyll</p>
          </Card>

          <Card>
            <h4>Healthy Points</h4>
            <h4>Experience</h4>
          </Card>

          <Features>
            <FeatureContainer>
              <SparkCicleFeature value='49' name='Defense' />
            </FeatureContainer>
            <FeatureContainer>
              <SparkCicleFeature value='165' name='Attack' />
            </FeatureContainer>
            <FeatureContainer>
              <SparkCicleFeature value='130' name='SP Attack' />
            </FeatureContainer>
            <FeatureContainer>
              <SparkCicleFeature value='271' name='Sp Defense' />
            </FeatureContainer>
          </Features>
        </Description>
      </CardContainer>
    </Container>
  )
}
