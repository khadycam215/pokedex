import React from 'react'
import { Container, Img, Description, ContainerProps } from './styles'

import { SparkCicleFeature } from '../SparkCircleFeature'
import { Tag } from '../Tag'
import { useNearScreen } from '../../hook/useNearScreen'

export const PreviewCard = (props) => {
  const {
    handleShowModal,
    name = 'Charizard',
    image = 'https://vignette.wikia.nocookie.net/es.pokemon/images/9/95/Charizard.png/revision/latest/scale-to-width-down/1000?cb=20180325003352',
    attack = 419,
    defense = 49,
    type1 = { name: 'Grass', color: '#30D97E' },
    type2 = { name: 'Poison', color: '#7A30D9' },
    pokedexNumber
  } = props

  const [show, ref] = useNearScreen()
  const handleClick = () => handleShowModal({ pokedexNumber: pokedexNumber })

  return (
    <Container ref={ref} onClick={handleClick}>
      {
        show &&
          <>
            <Description>
              <h2>{name}</h2>

              <ContainerProps>
                <SparkCicleFeature value={attack} name='Attack' />
                <SparkCicleFeature value={defense} name='Defense' />
              </ContainerProps>

              <ContainerProps>
                <Tag {...type1} />
                {type2.name !== 'no-type' && <Tag {...type2} />}
              </ContainerProps>
            </Description>

            <Img color={type1.color}>
              <img
                src={image}
                alt={`Imagen de poquemon ${name}`}
              />
            </Img>
          </>
      }
    </Container>
  )
}
