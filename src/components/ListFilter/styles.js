import styled from 'styled-components'

export const Dl = styled.dl`
  & dd {
    position: relative;
     & ul {
      position: absolute;
      top: 10px;
      z-index: 1;
      display: ${({ display }) => display};
      overflow: auto;
      width: 102px;
      height: 125px;
      padding-top: ${({ theme }) => theme.paddingM};
      padding-left: ${({ theme }) => theme.paddingB};
      border-radius: ${({ theme }) => theme.borderXs};
      box-shadow: ${({ theme }) => theme.shadowXs};
      background-color: ${({ theme }) => theme.colorGrey};
      list-style: none;
      & li {
        margin-bottom: ${({ theme }) => theme.spacingS};
        font-family: ${({ theme }) => theme.fontParagraphs};
        user-select: none;
        & input {
          margin-right: ${({ theme }) => theme.spacingS};
        }
      }
      &::-webkit-scrollbar {
        width: 10px;
      }

      &::-webkit-scrollbar-thumb {
        display: ${({ display }) => display};
        border-radius: ${({ theme }) => theme.borderS};
        background: ${({ theme }) => theme.colorWhiteLight};
        box-shadow: ${({ theme }) => theme.shadowB};
      }
    }
  }
`

export const FilterContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 135px;
  height: 25px;
  margin-right: ${({ theme }) => theme.spacingB};
  border-radius: ${({ theme }) => theme.borderXs};
  box-shadow: ${({ theme }) => theme.shadowXs};
  background-color: ${({ theme }) => theme.colorGrey};
  user-select: none;
  cursor: pointer;

  & p {
    text-transform: capitalize;
  }
  & svg {
    position: absolute;
    right: 10px;
  }
`
