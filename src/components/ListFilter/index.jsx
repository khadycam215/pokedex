import React, { useState } from 'react'
import { FilterContainer, Dl } from './styles'
import { MdExpandMore, MdKeyboardArrowUp } from 'react-icons/md'

export const ListFilter = (props) => {
  const {
    data: {
      loading,
      error,
      pokemonsTypesCatalog = []
    },
    open,
    handleOpen,
    handleFilter
  } = props

  const [type, setType] = useState('type')

  if (loading) return <p>Loading...</p>
  if (error) return <p>Error: Sorry there was a problem</p>

  const handleCheck = (e) => {
    e.target.checked = false
    handleOpen()

    if (e.target.value === 'no-type') {
      setType('type')
      handleFilter({})
      return
    }
    setType(e.target.value)
    handleFilter({ type1_Name: e.target.value })
  }

  return (
    <Dl display={open ? 'block' : 'none'}>
      <dt>
        <FilterContainer onClick={handleOpen}>
          <p>{type}</p>
          {open ? <MdKeyboardArrowUp /> : <MdExpandMore />}
        </FilterContainer>
      </dt>
      <dd>
        <ul>
          {pokemonsTypesCatalog
            .map(({ id, name }) =>
              <li key={id}>
                <input type='checkbox' value={name} onChange={handleCheck} />{name}
              </li>
            )}
        </ul>
      </dd>
    </Dl>
  )
}
