import React from 'react'
import { Container, Img, Span } from './styles'
import pokeball from '../../assets/static/pokeball.png'

export const Loading = () => {
  return (
    <Container>
      <Img>
        <img src={pokeball} alt='Loading cards' />
      </Img>
      <Span>
        <p>Loading...</p>
      </Span>
    </Container>
  )
}
