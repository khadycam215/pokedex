import React from 'react'
import { Link } from '@reach/router'
import { StyleNav, Ul, Item } from './styles'

export const Nav = () => {
  return (
    <StyleNav aria-label='Main Menu'>
      <Ul aria-label='Menu'>
        <Item>
          <Link tabIndex='0' aria-label='Home' to='/'>Home</Link>
        </Item>
        <Item>
          <Link tabIndex='0' aria-label='Pokemons Gallery' to='/pokedex'>Pokedex</Link>
        </Item>
        <Item>
          <Link tabIndex='0' aria-label='Legendary Pokemons' to='/legends'>Legends</Link>
        </Item>
        <Item>
          <Link tabIndex='0' aria-label='Documentation' to='/'>Documentation</Link>
        </Item>
      </Ul>
    </StyleNav>
  )
}
