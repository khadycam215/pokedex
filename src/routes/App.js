import React from 'react'
import { Router } from '@reach/router'

//* Styles
import { ThemeProvider } from 'styled-components'
import { variables } from '../styles/GlobalStyles'
import { GlobalStyles } from '../styles/styles'

//* Components
import { Layout } from '../components/Layout'
import { Home } from '../pages/Home'
import { Pokedex } from '../pages/Pokedex'
import { Legends } from '../pages/Legends'
import { NotFound } from '../pages/NotFound'
import { OurTeam } from '../pages/OurTeam'

export const App = () => {
  return (
    <ThemeProvider theme={variables}>
      <GlobalStyles />
      <Layout>
        <Router>
          <NotFound default />
          <Home path='/' />
          <Pokedex path='/pokedex' />
          <Legends path='/legends' />
          <OurTeam path='/team' />
        </Router>
      </Layout>
    </ThemeProvider>
  )
}
