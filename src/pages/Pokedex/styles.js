import styled from 'styled-components'

export const Main = styled.main`
  background: linear-gradient(${({ theme }) => theme.colorGrey}, ${({ theme }) => theme.colorWhiteLight});
`
