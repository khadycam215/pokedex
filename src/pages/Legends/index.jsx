import React from 'react'
import { Helmet } from 'react-helmet'
import { StylesLegends, Container } from './styles'
import { LegendaryHero } from '../../components/LegendaryHero'

export const Legends = () => {
  return (
    <StylesLegends>
      <Helmet>
        <meta
          charSet='utf-8'
          lang='en'
          name='Description'
          content='This section contains the most powerful characters in the pokemon world.'
        />
        <meta name='author' content='PokeSpartns' />
        <title>Legends</title>
      </Helmet>
      <Container>
        <LegendaryHero aria-label='Legends' section='Legends' />
        <LegendaryHero aria-label='Stronger' section='Stronger' />
        <LegendaryHero aria-label='Weaker' section='Weaker' />
      </Container>
    </StylesLegends>
  )
}
