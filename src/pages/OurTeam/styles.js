import styled from 'styled-components'

export const Section = styled.section`
display:flex;
flex-direction:column;
align-items:center;
justify-content:center;
background-color:${({ theme }) => theme.colorGrey};
`

export const Container = styled.div`
width: 85%;
box-sizing: border-box;
margin: ${({ theme }) => theme.spacingB} auto;
max-width: ${({ theme }) => theme.containerMaxWidth};

& > h1 {
font-size: ${({ theme }) => theme.sizeFontTitle};
}

& > p {
font-size: ${({ theme }) => theme.sizeFontsH3};
margin-bottom:${({ theme }) => theme.spacingB};
}

@media(max-width:425px){
&>*{
text-align:center;
}
}
`

export const Grid = styled.div`
display:grid;
grid-template-columns: 1fr 1fr;
grid-gap:72px;
margin: 0 auto;
min-height:calc(100vh - 125px);
width:80%;

@media(max-width:1024px){
width:90%;
}

@media(max-width:768px){
width:100%;
}


@media(max-width:425px){
grid-template-columns:1fr;
}

`
