import React from 'react'
import { Helmet } from 'react-helmet'

import { Section, Container, Grid } from './styles'
import { TeamCard } from '../../components/TeamCard'
import emmanuelPhoto from '../../assets/static/emmanuelPhoto.png'
import eliasPhoto from '../../assets/static/eliasPhoto.png'
import estebanPhoto from '../../assets/static/estebanPhoto.jpg'
export const OurTeam = () => {
  return (
    <Section>
      <Helmet>
        <meta
          charSet='utf-8'
          lang='en'
          name='Description'
          content='We introduce you to the creators of this page. We are 4 partners of the Platzi Master program'
        />
        <meta name='author' content='PokeSpartns' />
        <title>OurTeam</title>
      </Helmet>
      <Container>
        <h1>Our Tems</h1>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aeneancommodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quamfelis, ultricies nec, pellentesque eu, pretium quis, sem. Nullaconsequat massa quis enim.
        </p>

        <Grid>
          <TeamCard
            src={emmanuelPhoto}
            alt='Frontend developer and designer'
            name='Emmanuel Garcia'
            description='I am a front end developer with knowledge in UI and UX. I am an orderly and disciplined person who seeks to provide the best user experience through web applications.
                        I work with HTML, CSS (including preprocessors like Stylus, SASS and PUG), and Javascript vanilla
                        I have experience working with Frameworks like React, Bootstrap and Foundation as well as Illustrator and Photoshop.
                        Platzi Master has allowed me to enhance my knowledge through both individual and group projects, which not only increased my technical knowledge, but also my soft skills such as teamwork, team communication and documentation.'
            srcSmHb='https://github.com/EmmaIsWorking'
            srcSmT='https://twitter.com/emmaisworking'
          />

          <TeamCard
            src={estebanPhoto}
            alt='Frontend developer and designer'
            name='Esteban Ladino'
            description="I’m Esteban, I' m from Colombia and I’m a frontend developer, I specialize in React apps, CSS and HTML. In addition, I work with various frameworks like Vue, Svelte and CSS preprocessors. I am interested in building innovative interfaces which proved the best user experience possible.
                          My professional life has been 100% driven by my passion for video-games and system engineering.
                          Right now, I’m focusing on 3D rendering.I'm interested in creating 3D browser designs using canvas and threeJS. I am aiming to become a full-stack developer in the near future."
            srcSmHb='https://github.com/Esteban-Ladino'
            srcSmT=''
          />

          <TeamCard
            src={eliasPhoto}
            alt='Frontend developer and designer'
            name='Elias Ojeda Medina'
            description='Software engineer - DataScientist. I am a Software Engineer and Data Scientist, with experience in developing software with different languages, currently using Python as the main language, an active member of the Platzi Master program. I collaborated independently in the sectors: real estate, restaurant and retail in the development of computer solutions for certain processes and in the construction of web platforms, implementing recent technologies such as cloud computing, graphql and SPAs. In recent years, he worked in data processing for the health sector, supporting data centralization processes, analysis and visualization of information for decision-making and improvement of both clinical and management and operation processes, working with implementations of Robust software with systems like SAP. I like to participate in technology events, business, outdoor activities, collaborate, read and learn about different topics.'
            srcSmHb='https://github.com/eocode'
            srcSmT='https://twitter.com/eocode'
          />

          <TeamCard
            src={emmanuelPhoto}
            alt='Frontend developer and designer'
            name='Cristhian Castillo'
            description='Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.'
            srcSmHb='https://github.com/KorKux1'
          />
        </Grid>
      </Container>
    </Section>
  )
}
